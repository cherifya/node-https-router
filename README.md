### node-https-router
A node.js requests router that supports multiple SSL domains on the same IP using SNI.
It's a thin wrapper around (http-proxy)[https://github.com/http-party/node-http-proxy/].